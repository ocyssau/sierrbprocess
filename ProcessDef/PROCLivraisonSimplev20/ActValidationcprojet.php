<?php

namespace ProcessDef\PROCLivraisonSimplev20;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity validationcprojet
 *
 * @package LivraisonSimple20
 * @generated 2018-08-28T17:42:12+0200
 * @author
 */
class ActValidationcprojet extends AbstractActivity
{

    /**
     * {@inheritDoc}
     * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger()
     */
    public function trigger(\Workflow\Model\Event $event)
    {
        parent::trigger($event);
    }


}

