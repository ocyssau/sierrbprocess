<?php

namespace ProcessDef\PROCLivraisonSimplev20;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity validationdesigner
 *
 * @package LivraisonSimple20
 * @generated 2018-08-28T17:42:12+0200
 * @author
 */
class ActValidationdesigner extends AbstractActivity
{

    /**
     * {@inheritDoc}
     * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger()
     */
    public function trigger(\Workflow\Model\Event $event)
    {
		/** @var \Rbplm\Ged\Document\Version $document */
		$document = $event->getEmitter()->document;
		$document->lock(\Rbplm\Ged\AccessCode::INWORKFLOW);
		$document->dao->save($document);
                parent::trigger($event);
    }


}