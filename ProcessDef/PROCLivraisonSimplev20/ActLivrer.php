<?php
namespace ProcessDef\PROCLivraisonSimplev20;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity livrer
 *
 * @package LivraisonSimple20
 * @generated 2017-02-22T15:23:09+0100
 * @author
 */
class ActLivrer extends AbstractActivity
{

	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger($e)
	 */
	public function trigger(\Workflow\Model\Event $event)
	{
		parent::trigger($event);
	}
}

