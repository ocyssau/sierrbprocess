<?php
namespace ProcessDef\PROCPartsLivrablesv20;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity abort
 *
 * @package PartsLivrables20
 * @generated 2017-07-05T13:05:10+0200
 * @author
 */
class ActAbort extends AbstractActivity
{

	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger()
	 */
	public function trigger(\Workflow\Model\Event $event)
	{
		parent::trigger($event);
	}
}

