<?php
namespace ProcessDef\PROCPartsLivrablesv10;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity End
 *
 * @package PartsLivrables10
 * @generated 2017-02-23T11:10:14+0100
 * @author
 */
class ActEnd extends AbstractActivity
{

	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger($e)
	 */
	public function trigger(\Workflow\Model\Event $event)
	{
		/** @var \Rbplm\Ged\Document\Version $document */
		$document = $event->getEmitter()->document;
		$document->lock(\Rbplm\Ged\AccessCode::VALIDATE);
		$document->dao->save($document);
		return parent::trigger($event);
	}
}

