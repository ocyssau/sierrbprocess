<?php
namespace ProcessDef\PROCPartsLivrablesv10;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity Livrer
 *
 * @package PartsLivrables10
 * @generated 2017-02-23T11:10:14+0100
 * @author
 */
class ActLivrer extends AbstractActivity
{

	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger($e)
	 */
	public function trigger(\Workflow\Model\Event $event)
	{
		parent::trigger($event);
	}
}

