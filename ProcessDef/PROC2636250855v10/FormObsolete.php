<?php

namespace ProcessDef\PROC2636250855v10;

use Docflow\Service\Workflow\Callback\AbstractForm;

/**
 * A class for activity form obsolete
 *
 * @package 263625085510
 * @generated 2018-03-20T16:05:50+0100
 * @author
 */
class FormObsolete extends AbstractForm
{

    const BIND_ON_VALIDATE = 0;

    const BIND_MANUAL = 1;

    const VALIDATE_ALL = 16;

    const VALUES_NORMALIZED = 17;

    const VALUES_RAW = 18;

    const VALUES_AS_ARRAY = 19;


}

