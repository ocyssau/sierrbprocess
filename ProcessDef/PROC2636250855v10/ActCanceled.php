<?php

namespace ProcessDef\PROC2636250855v10;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity canceled
 *
 * @package 263625085510
 * @generated 2018-03-20T16:05:50+0100
 * @author
 */
class ActCanceled extends AbstractActivity
{

    /**
     * {@inheritDoc}
     * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger()
     */
    public function trigger(\Workflow\Model\Event $event)
    {
        parent::trigger($event);
    }


}

