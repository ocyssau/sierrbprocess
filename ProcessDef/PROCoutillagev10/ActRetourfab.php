<?php
namespace ProcessDef\PROCoutillagev10;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity retour_fab
 *
 * @package outillage10
 * @generated 2017-06-23T17:16:52+0200
 * @author
 */
class ActRetourfab extends AbstractActivity
{

	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger($e)
	 */
	public function trigger(\Workflow\Model\Event $event)
	{
		parent::trigger($event);
	}
}

