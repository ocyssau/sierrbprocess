<?php
namespace ProcessDef\PROCVieDesNormesv10;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity archiver
 *
 * @package VieDesNormes10
 * @generated 2017-08-21T17:16:24+0200
 * @author
 */
class ActArchiver extends AbstractActivity
{

	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger()
	 */
	public function trigger(\Workflow\Model\Event $event)
	{
		parent::trigger($event);
	}
}