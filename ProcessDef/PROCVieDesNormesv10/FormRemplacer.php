<?php
namespace ProcessDef\PROCVieDesNormesv10;

use Docflow\Service\Workflow\Callback\AbstractForm;

/**
 * A class for activity form Remplacer
 *
 * @package VieDesNormes10
 * @generated 2017-07-05T12:55:30+0200
 * @author
 */
class FormRemplacer extends AbstractForm
{

	const BIND_ON_VALIDATE = 0;

	const BIND_MANUAL = 1;

	const VALIDATE_ALL = 16;

	const VALUES_NORMALIZED = 17;

	const VALUES_RAW = 18;

	const VALUES_AS_ARRAY = 19;
}

