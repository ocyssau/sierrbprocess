<?php
namespace ProcessDef\PROCVieDesNormesv10;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity Remplacer
 *
 * @package VieDesNormes10
 * @generated 2017-07-05T12:55:30+0200
 * @author
 */
class ActRemplacer extends AbstractActivity
{

	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger()
	 */
	public function trigger(\Workflow\Model\Event $event)
	{
		parent::trigger($event);
	}
}

