<?php

namespace ProcessDef\PROC222995071v10;

use Docflow\Service\Workflow\Callback\AbstractActivity;
use Rbplm\People;

/**
 * A class for activity validationcve
 *
 * @package 228851831510
 * @generated 2018-01-09T09:44:54+0100
 * @author
 */
class ActValidationcve extends AbstractActivity
{
	
	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger()
	 */
	public function trigger(\Workflow\Model\Event $event)
	{
		/** @var \Rbplm\Ged\Document\Version $document */
		$document = $event->getEmitter()->document;
		$document->lock(\Rbplm\Ged\AccessCode::INWORKFLOW);
		$document->dao->save($document);
		
		/* Helper init */
		$helper = new \ProcessDef\ActivityHelper($this);
		
		/* email to this users */
		$users = array(
			'r_david'
		);
		
		/* Get urls from helper */
		$docDetailUrl = $helper->getDetailUrl($document);
		$runNextActivityUrl = $helper->getRunActivityUrl($document);
		
		/* Get current user */
		$from = People\CurrentUser::get();
		
		/* Subject of mail */
		$subject = sprintf(tra('Validation cve evt of document %s by %s'), $document->getNumber(), $from->getLogin());
		
		/* Text body of mail */
		$htmlBody = sprintf('<p> The document %s has been validated by user %s </p>', $document->getNumber(), $from->getLogin());
		$htmlBody .= '<a class="rb-popup btn btn-default" href="' . $docDetailUrl . '">Get Details of document' . $document->getNumber() . '</a>';
		$htmlBody .= '<a class="rb-popup btn btn-default" href="' . $runNextActivityUrl . '">Run next activity</a>';
		
		/* Send mail */
		$helper->notify($users, $subject, $htmlBody);
		
		parent::trigger($event);
	}
}
