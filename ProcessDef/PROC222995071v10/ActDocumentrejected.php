<?php

namespace ProcessDef\PROC222995071v10;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity documentrejected
 *
 * @package 228851831510
 * @generated 2018-01-09T13:56:22+0100
 * @author
 */
class ActDocumentrejected extends AbstractActivity
{
	
	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger()
	 */
	public function trigger(\Workflow\Model\Event $event)
	{
		/** @var \Rbplm\Ged\Document\Version $document */
		$document = $event->getEmitter()->document;
		$document->lock(\Rbplm\Ged\AccessCode::FREE);
		$document->dao->save($document);
		parent::trigger($event);
	}
}
