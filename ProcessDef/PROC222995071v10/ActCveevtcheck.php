<?php
namespace ProcessDef\PROC222995071v10;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity cveevtcheck
 *
 * @package 22299507110
 * @generated 2018-01-11T15:00:35+0100
 * @author
 */
class ActCveevtcheck extends AbstractActivity
{

	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger()
	 */
	public function trigger(\Workflow\Model\Event $event)
	{
		parent::trigger($event);
	}
}

