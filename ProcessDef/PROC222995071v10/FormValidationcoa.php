<?php

namespace ProcessDef\PROC222995071v10;

use Docflow\Service\Workflow\Callback\AbstractForm;

/**
 * A class for activity form validationcoa
 *
 * @package 22299507110
 * @generated 2018-01-11T15:49:42+0100
 * @author
 */
class FormValidationcoa extends AbstractForm
{

    const BIND_ON_VALIDATE = 0;

    const BIND_MANUAL = 1;

    const VALIDATE_ALL = 16;

    const VALUES_NORMALIZED = 17;

    const VALUES_RAW = 18;

    const VALUES_AS_ARRAY = 19;


}

