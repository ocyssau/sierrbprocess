<?php
namespace ProcessDef\PROCsuivi08415v10;

use Docflow\Service\Workflow\Callback\AbstractForm;

/**
 * A class for activity form rejeter_annulation_odf
 *
 * @package suivi0841510
 * @generated 2017-07-05T12:57:24+0200
 * @author
 */
class FormRejeterannulationodf extends AbstractForm
{

	const BIND_ON_VALIDATE = 0;

	const BIND_MANUAL = 1;

	const VALIDATE_ALL = 16;

	const VALUES_NORMALIZED = 17;

	const VALUES_RAW = 18;

	const VALUES_AS_ARRAY = 19;
}

