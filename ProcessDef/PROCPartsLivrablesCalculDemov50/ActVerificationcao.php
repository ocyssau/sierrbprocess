<?php
namespace ProcessDef\PROCPartsLivrablesCalculDemov50;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity verification_cao
 *
 * @package PartsLivrablesCalculDemo50
 * @generated 2017-02-21T17:45:22+0100
 * @author
 */
class ActVerificationcao extends AbstractActivity
{

	public function trigger(\Workflow\Model\Event $event)
	{
		parent::trigger($event);
	}
}

