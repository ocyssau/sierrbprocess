<?php
namespace ProcessDef\PROCPartsLivrablesCalculDemov50;

use Docflow\Service\Workflow\Callback\AbstractForm;

/**
 * A class for activity form confirm_rt
 *
 * @package PartsLivrablesCalculDemo50
 * @generated 2017-02-21T17:45:22+0100
 * @author
 */
class FormConfirmrt extends AbstractForm
{
}

