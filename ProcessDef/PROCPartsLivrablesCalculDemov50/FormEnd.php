<?php
namespace ProcessDef\PROCPartsLivrablesCalculDemov50;

use Docflow\Service\Workflow\Callback\AbstractForm;

/**
 * A class for activity form end
 *
 * @package PartsLivrablesCalculDemo50
 * @generated 2017-02-21T17:45:22+0100
 * @author
 */
class FormEnd extends AbstractForm
{
}

