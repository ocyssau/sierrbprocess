<?php
namespace ProcessDef\PROCPartsLivrablesCalculDemov50;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity livrer
 *
 * @package PartsLivrablesCalculDemo50
 * @generated 2017-02-21T17:45:21+0100
 * @author
 */
class ActLivrer extends AbstractActivity
{

	public function trigger(\Workflow\Model\Event $event)
	{
		parent::trigger($event);
	}
}

