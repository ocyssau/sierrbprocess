<?php
namespace ProcessDef\PROCPartsLivrablesCalculDemov50;

use Docflow\Service\Workflow\Callback\AbstractForm;

/**
 * A class for activity form abort
 *
 * @package outillage20
 * @generated 2017-07-03T13:34:16+0200
 * @author
 */
class FormAbort extends AbstractForm
{
}

