<?php
namespace ProcessDef\PROCsuiviValidationCalculMeublesv10;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity valider
 *
 * @package suiviValidationCalculMeubles10
 * @generated 2017-07-04T17:55:56+0200
 * @author
 */
class ActValider extends AbstractActivity
{

	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger($e)
	 */
	public function trigger(\Workflow\Model\Event $event)
	{
		parent::trigger($event);
	}
}

