<?php
namespace ProcessDef\PROCPartsLivrablesCalculv30;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity end
 *
 * @package PartsLivrablesCalcul30
 * @generated 2017-02-22T11:07:20+0100
 * @author
 */
class ActEnd extends AbstractActivity
{

	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger($e)
	 */
	public function trigger(\Workflow\Model\Event $event)
	{
		/** @var \Rbplm\Ged\Document\Version $document */
		$document = $event->getEmitter()->document;
		$document->lock(\Rbplm\Ged\AccessCode::VALIDATE);
		$document->dao->save($document);
		
		parent::trigger($event);
	}
}

