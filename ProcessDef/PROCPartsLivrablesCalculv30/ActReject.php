<?php
namespace ProcessDef\PROCPartsLivrablesCalculv30;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity reject
 *
 * @package PartsLivrablesCalcul30
 * @generated 2017-07-05T09:18:12+0200
 * @author
 */
class ActReject extends AbstractActivity
{

	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger($e)
	 */
	public function trigger(\Workflow\Model\Event $event)
	{
		/** @var \Rbplm\Ged\Document\Version $document */
		$document = $event->getEmitter()->document;
		$document->unlock();
		$document->dao->save($document);
		parent::trigger($event);
	}
}

