<?php
namespace ProcessDef\PROCPartsLivrablesCalculv30;

use Docflow\Service\Workflow\Callback\AbstractForm;

/**
 * A class for activity form verification_calcul
 *
 * @package PartsLivrablesCalcul30
 * @generated 2017-02-22T11:07:20+0100
 * @author
 */
class FormVerificationcalcul extends AbstractForm
{

	const BIND_ON_VALIDATE = 0;

	const BIND_MANUAL = 1;

	const VALIDATE_ALL = 16;

	const VALUES_NORMALIZED = 17;

	const VALUES_RAW = 18;

	const VALUES_AS_ARRAY = 19;
}

