<?php
namespace ProcessDef\PROCPartsLivrablesCalculv30;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity verification_calcul
 *
 * @package PartsLivrablesCalcul30
 * @generated 2017-02-22T11:07:20+0100
 * @author
 */
class ActVerificationcalcul extends AbstractActivity
{

	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger($e)
	 */
	public function trigger(\Workflow\Model\Event $event)
	{
		parent::trigger($event);
	}
}

