<?php
namespace ProcessDef\PROCPartsLivrablesCalculv30;

use Docflow\Service\Workflow\Callback\AbstractActivity;
use Workflow\Model\Wf;

/**
 * A class for activity abort
 *
 * @package PartsLivrablesCalcul30
 * @generated 2017-02-22T11:07:20+0100
 * @author
 */
class ActAbort extends AbstractActivity
{

	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger($e)
	 */
	public function trigger(\Workflow\Model\Event $event)
	{
		$document = $event->getEmitter()->document;
		$factory = $this->document->dao->factory;
		$procInst = $event->getEmitter()->instance;
		
		/* close current instance */
		$procInst->setStatus(Wf\Instance::STATUS_ABORTED)->setEnded(new \DateTime());
		$factory->getDao($procInst)->save($procInst);
		
		/** @var \Rbplm\Ged\Document\Version $document */
		$document->lifeStage = 'aborted';
		$document->unlock();
		$document->dao->save($document);
		
		parent::trigger($event);
	}
}

