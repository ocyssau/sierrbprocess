<?php

namespace ProcessDef\PROC3692255212v10;

use Docflow\Service\Workflow\Callback\AbstractActivity;
use Docflow\Service\Workflow\Callback\CallbackException;
use Workflow\Model\Wf;

/**
 * A class for activity abortedvalidation
 *
 * @package 228851831510
 * @generated 2018-01-09T14:01:50+0100
 * @author
 */
class ActAbortedvalidation extends AbstractActivity
{
	
	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger()
	 */
	public function trigger(\Workflow\Model\Event $event)
	{
		$document = $event->getEmitter()->document;
		$factory = $this->document->dao->factory;
		$procInst = $event->getEmitter()->instance;
		
		if($document->lifeStage != 'CVE check requested'){
			throw new CallbackException('You can abort only documents with lifeStage = "CVE check requested"');
		}
		
		/** @var \Rbs\Wf\Instance\DocumentLinkDao $wfDocLinkDao */
		//$wfDocLinkDao = $factory->getDao(\Rbs\Wf\Instance\DocumentLink::$classId);
		//$wfDocLinkDao->deleteFromDocumentId($this->document->getId());
		
		/* close current instance */
		$procInst->setStatus(Wf\Instance::STATUS_ABORTED)->setEnded(new \DateTime());
		$factory->getDao($procInst::$classId)->save($procInst);
		
		/** @var \Rbplm\Ged\Document\Version $document */
		$document->lifeStage = 'aborted';
		$document->unlock();
		$document->dao->save($document);
		
		parent::trigger($event);
	}
}
