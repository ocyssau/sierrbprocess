<?php

namespace ProcessDef\PROC3692255212v10;

use Docflow\Service\Workflow\Callback\AbstractForm;

/**
 * A class for activity form validationcve
 *
 * @package 369225521210
 * @generated 2018-01-11T14:56:30+0100
 * @author
 */
class FormValidationcve extends AbstractForm
{

    const BIND_ON_VALIDATE = 0;

    const BIND_MANUAL = 1;

    const VALIDATE_ALL = 16;

    const VALUES_NORMALIZED = 17;

    const VALUES_RAW = 18;

    const VALUES_AS_ARRAY = 19;


}

