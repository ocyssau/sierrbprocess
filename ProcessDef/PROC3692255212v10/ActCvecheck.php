<?php

namespace ProcessDef\PROC3692255212v10;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity cvecheck
 *
 * @package 369225521210
 * @generated 2018-01-11T14:56:30+0100
 * @author
 */
class ActCvecheck extends AbstractActivity
{

    /**
     * {@inheritDoc}
     * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger()
     */
    public function trigger(\Workflow\Model\Event $event)
    {
        parent::trigger($event);
    }


}

