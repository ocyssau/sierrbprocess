<?php
namespace ProcessDef\PROC3170769154v10;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity projectmanagerreject
 *
 * @package 317076915410
 * @generated 2018-01-09T14:47:05+0100
 * @author
 */
class ActProjectmanagerreject extends AbstractActivity
{

	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger()
	 */
	public function trigger(\Workflow\Model\Event $event)
	{
		/** @var \Rbplm\Ged\Document\Version $document */
		$document = $event->getEmitter()->document;
		$document->lock(\Rbplm\Ged\AccessCode::FREE);
		$document->dao->save($document);
		parent::trigger($event);
	}
}
