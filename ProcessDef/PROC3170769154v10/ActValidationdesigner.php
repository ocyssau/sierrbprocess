<?php
namespace ProcessDef\PROC3170769154v10;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity validationdesigner
 *
 * @package 317076915410
 * @generated 2018-01-09T14:27:44+0100
 * @author
 */
class ActValidationdesigner extends AbstractActivity
{

	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger()
	 */
	public function trigger(\Workflow\Model\Event $event)
	{
		/** @var \Rbplm\Ged\Document\Version $document */
		$document = $event->getEmitter()->document;
		$document->lock(\Rbplm\Ged\AccessCode::INWORKFLOW);
		
		/* Verifie la presence et les dates du pdf */
		$docfiles = $document->getDocfiles();
		
		/** @var \Rbplm\Ged\Docfile\Version $mainfile */
		$mainfile = reset($docfiles);
		
		/* CATDrawing must have pdf */
		if ( $mainfile && $mainfile->getData()->extension == '.CATDrawing' ) {
			$helper = new \ProcessDef\ActivityHelper($this);
			try {
				$helper->checkDate($document, '.pdf', 30);
			}
			catch( InformationException $exception ) {
				if ( $exception->getCode() == ActivityHelper::ERRCODE_NOFILE ) {
					null;
				}
			}
			catch( CallbackException $exception ) {
				$event->error[] = $exception->getMessage();
			}
		}

		/* */
		$document->dao->save($document);
		parent::trigger($event);
	}
}
