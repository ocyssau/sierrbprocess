<?php
namespace ProcessDef\PROC3170769154v10;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity validationcprojet
 *
 * @package 317076915410
 * @generated 2018-01-09T14:28:32+0100
 * @author
 */
class ActValidationcprojet extends AbstractActivity
{

	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger()
	 */
	public function trigger(\Workflow\Model\Event $event)
	{
		parent::trigger($event);
	}
}

