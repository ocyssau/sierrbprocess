<?php
namespace ProcessDef\PROC3170769154v10;

use Docflow\Service\Workflow\Callback\AbstractForm;

/**
 * A class for activity form validationcalcul
 *
 * @package 317076915410
 * @generated 2018-01-09T14:34:58+0100
 * @author
 */
class FormValidationcalcul extends AbstractForm
{

	const BIND_ON_VALIDATE = 0;

	const BIND_MANUAL = 1;

	const VALIDATE_ALL = 16;

	const VALUES_NORMALIZED = 17;

	const VALUES_RAW = 18;

	const VALUES_AS_ARRAY = 19;
}

