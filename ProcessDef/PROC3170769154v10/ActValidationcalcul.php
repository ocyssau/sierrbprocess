<?php
namespace ProcessDef\PROC3170769154v10;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity validationcalcul
 *
 * @package 317076915410
 * @generated 2018-01-09T14:34:58+0100
 * @author
 */
class ActValidationcalcul extends AbstractActivity
{

	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger()
	 */
	public function trigger(\Workflow\Model\Event $event)
	{
		parent::trigger($event);
	}
}
