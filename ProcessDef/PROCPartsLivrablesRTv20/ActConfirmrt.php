<?php
namespace ProcessDef\PROCPartsLivrablesRTv20;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity confirm_rt
 *
 * @package PartsLivrablesRT20
 * @generated 2017-07-05T13:08:50+0200
 * @author
 */
class ActConfirmrt extends AbstractActivity
{
}

