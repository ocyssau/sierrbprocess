<?php
namespace ProcessDef\PROCPartsLivrablesRTv20;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity abort
 *
 * @package PartsLivrablesRT20
 * @generated 2017-07-05T13:08:50+0200
 * @author
 */
class ActAbort extends AbstractActivity
{
}

