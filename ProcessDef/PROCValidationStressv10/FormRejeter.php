<?php
namespace ProcessDef\PROCValidationStressv10;

use Docflow\Service\Workflow\Callback\AbstractForm;

/**
 * A class for activity form rejeter
 *
 * @package ValidationStress10
 * @generated 2017-09-18T15:44:52+0200
 * @author
 */
class FormRejeter extends AbstractForm
{

	const BIND_ON_VALIDATE = 0;

	const BIND_MANUAL = 1;

	const VALIDATE_ALL = 16;

	const VALUES_NORMALIZED = 17;

	const VALUES_RAW = 18;

	const VALUES_AS_ARRAY = 19;
}

