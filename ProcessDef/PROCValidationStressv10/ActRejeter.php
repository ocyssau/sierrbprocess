<?php
namespace ProcessDef\PROCValidationStressv10;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity rejeter
 *
 * @package ValidationStress10
 * @generated 2017-09-18T15:44:52+0200
 * @author
 */
class ActRejeter extends AbstractActivity
{

	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger()
	 */
	public function trigger(\Workflow\Model\Event $event)
	{
		$document = $event->getEmitter()->document;
		$document->lock(\Rbplm\Ged\AccessCode::FREE);
		$document->dao->save($document);
		parent::trigger($event);
	}
}

