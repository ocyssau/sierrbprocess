<?php
namespace ProcessDef\PROCValidationStressv10;

use Docflow\Service\Workflow\Callback\AbstractActivity;
use Docflow\Service\Workflow\Callback\CallbackException;
use Workflow\Model\Wf;

/**
 * A class for activity abort
 *
 * @package ValidationStress10
 * @generated 2017-07-05T13:02:03+0200
 * @author
 */
class ActAbort extends AbstractActivity
{

	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger()
	 */
	public function trigger(\Workflow\Model\Event $event)
	{
		throw new CallbackException('This activity is unactivated');
		
		$document = $event->getEmitter()->document;
		$factory = $this->document->dao->factory;
		$procInst = $event->getEmitter()->instance;
		
		/** @var \Rbs\Wf\Instance\DocumentLinkDao $wfDocLinkDao */
		//$wfDocLinkDao = $factory->getDao(\Rbs\Wf\Instance\DocumentLink::$classId);
		//$wfDocLinkDao->deleteFromDocumentId($this->document->getId());
		
		/* close current instance */
		$procInst->setStatus(Wf\Instance::STATUS_ABORTED)->setEnded(new \DateTime());
		$factory->getDao($procInst::$classId)->save($procInst);
		
		/** @var \Rbplm\Ged\Document\Version $document */
		$document->lifeStage = 'aborted';
		$document->unlock();
		$document->dao->save($document);
		
		parent::trigger($event);
	}
}

