<?php
namespace ProcessDef\PROCValidationStressv10;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity Verifier
 *
 * @package ValidationStress10
 * @generated 2017-07-05T13:02:03+0200
 * @author
 */
class ActVerifier extends AbstractActivity
{

	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger()
	 */
	public function trigger(\Workflow\Model\Event $event)
	{
		parent::trigger($event);
	}
}