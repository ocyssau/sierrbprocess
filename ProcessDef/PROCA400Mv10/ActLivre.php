<?php
namespace ProcessDef\PROCA400Mv10;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity livre
 *
 * @package A400M10
 * @generated 2017-07-05T13:03:58+0200
 * @author
 */
class ActLivre extends AbstractActivity
{

	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger()
	 */
	public function trigger(\Workflow\Model\Event $event)
	{
		parent::trigger($event);
	}
}

