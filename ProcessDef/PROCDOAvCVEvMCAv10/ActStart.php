<?php
namespace ProcessDef\PROCDOAvCVEvMCAv10;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity start
 *
 * @package DOACVEMCA10
 * @generated 2018-01-05T13:56:28+0100
 * @author
 */
class ActStart extends AbstractActivity
{

	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger()
	 */
	public function trigger(\Workflow\Model\Event $event)
	{
		parent::trigger($event);
	}
}