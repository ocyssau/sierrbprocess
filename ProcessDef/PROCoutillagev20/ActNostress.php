<?php
namespace ProcessDef\PROCoutillagev20;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity nostress
 *
 * @package outillage20
 * @generated 2017-11-02T16:32:05+0100
 * @author
 */
class ActNostress extends AbstractActivity
{

	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger()
	 */
	public function trigger(\Workflow\Model\Event $event)
	{
		parent::trigger($event);
	}
}
