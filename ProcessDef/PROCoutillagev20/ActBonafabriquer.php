<?php
namespace ProcessDef\PROCoutillagev20;

use Docflow\Service\Workflow\Callback\AbstractActivity;
use Docflow\Service\Workflow\Callback\InformationException;
use Docflow\Service\Workflow\Callback\CallbackException;
use ProcessDef\ActivityHelper;

/**
 * A class for activity bon_a_fabriquer
 *
 * @package outillage20
 *          @generated 2017-07-03T13:34:16+0200
 * @author
 *
 */
class ActBonafabriquer extends AbstractActivity
{

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger($e)
	 *
	 * @var \Workflow\Model\Event $event
	 *
	 */
	public function trigger(\Workflow\Model\Event $event)
	{
		/** @var \Rbplm\Ged\Document\Version $document */
		$document = $event->getEmitter()->document;
		$document->lock(\Rbplm\Ged\AccessCode::INWORKFLOW);
		
		/* Verifie la presence et les dates du pdf */
		$docfiles = $document->getDocfiles();
		/** @var \Rbplm\Ged\Docfile\Version $mainfile */
		$mainfile = reset($docfiles);
		
		/* CATDrawing must have pdf */
		if ( $mainfile && $mainfile->getData()->extension == '.CATDrawing' ) {
			$helper = new \ProcessDef\ActivityHelper($this);
			try {
				$helper->checkDate($document, '.pdf', 30);
			}
			catch( InformationException $exception ) {
				if ( $exception->getCode() == ActivityHelper::ERRCODE_NOFILE ) {
					null;
				}
			}
			catch( CallbackException $exception ) {
				$event->error[] = $exception->getMessage();
			}
		}
		
		/* */
		$document->dao->save($document);
		parent::trigger($event);
	}
}
