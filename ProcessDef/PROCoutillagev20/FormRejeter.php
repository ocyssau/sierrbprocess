<?php
namespace ProcessDef\PROCoutillagev20;

use Zend\Form\Form;

/**
 * A class for activity form rejeter
 *
 * @package outillage20
 * @generated 2017-07-03T13:34:16+0200
 * @author
 */
class FormRejeter extends Form
{

	/**
	 * @var string
	 */
	public $template;

	/**
	 * @var \Docflow\Service\Workflow
	 */
	public $workflow;

	/**
	 * @var string
	 */
	public $id;

	const BIND_ON_VALIDATE = 0;

	const BIND_MANUAL = 1;

	const VALIDATE_ALL = 16;

	const VALUES_NORMALIZED = 17;

	const VALUES_RAW = 18;

	const VALUES_AS_ARRAY = 19;

	/**
	 *
	 * @param \Docflow\Service\Workflow $workflow
	 */
	public function __construct($workflow)
	{
		/* we want to ignore the name passed */
		parent::__construct('activity-act1');
		$this->setAttribute('method', 'post');
		$this->workflow = $workflow;
		$id = 'outillagev20formrejeter';
		$this->id = $id;
		
		$this->add(array(
			'name' => $id,
			'attributes' => array(
				'type' => 'hidden',
				'value' => $id
			)
		));
		
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden'
			)
		));
		
		$this->add(array(
			'name' => 'comment',
			'attributes' => array(
				'type' => 'textarea',
				'class' => 'form-control'
			),
			'options' => array(
				'label' => 'Reason of reject'
			)
		));
		
		$this->add(array(
			'name' => 'submit',
			'attributes' => array(
				'type' => 'submit',
				'value' => 'Save',
				'id' => 'submitbutton'
			)
		));
	}
}
