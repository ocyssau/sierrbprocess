<?php
namespace ProcessDef\PROCoutillagev20;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity livrer
 *
 * @package outillage20
 * @generated 2017-07-03T13:34:16+0200
 * @author
 */
class ActLivrer extends AbstractActivity
{

	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger($e)
	 */
	public function trigger(\Workflow\Model\Event $event)
	{
		parent::trigger($event);
	}
}