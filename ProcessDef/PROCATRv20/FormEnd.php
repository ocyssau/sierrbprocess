<?php
namespace ProcessDef\PROCATRv20;

use Docflow\Service\Workflow\Callback\AbstractForm;

/**
 * A class for activity form end
 *
 * @package ATR20
 * @generated 2017-07-05T13:03:18+0200
 * @author
 */
class FormEnd extends AbstractForm
{

	const BIND_ON_VALIDATE = 0;

	const BIND_MANUAL = 1;

	const VALIDATE_ALL = 16;

	const VALUES_NORMALIZED = 17;

	const VALUES_RAW = 18;

	const VALUES_AS_ARRAY = 19;
}

