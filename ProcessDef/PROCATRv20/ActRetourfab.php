<?php
namespace ProcessDef\PROCATRv20;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity retour_fab
 *
 * @package ATR20
 * @generated 2017-07-05T13:03:18+0200
 * @author
 */
class ActRetourfab extends AbstractActivity
{

	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger()
	 */
	public function trigger(\Workflow\Model\Event $event)
	{
		parent::trigger($event);
	}
}

