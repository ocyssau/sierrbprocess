// %LICENCE_HEADER% namespace ProcessDef; use
Docflow\Service\Workflow\Callback\AbstractActivity; use
Rbplm\Ged\Document; use
Docflow\Service\Workflow\Callback\InformationException; use
Docflow\Service\Workflow\Callback\CallbackException; /** */ class
RtHelper { const ERRCODE_OLDER = 10000; const ERRCODE_NOFILE= 10010; /**
* * @var AbstractActivity */ protected $activity; /** * * @param
AbstractActivity $activity */ public function
__construct(AbstractActivity $activity) { $this->activity = $activity; }

/** * * Verifie les refus * * @param Document\Version $document *
@return string|false Message to display */ public function
checkRt(Document\Version $document) { $msg = true; $rt = new Rt();

$document = $document->GetProperty('document_version'); $doctypeId =
$document->GetProperty('doctype_id'); if ( $doctypeId ==
Rt::$_doctype_id ) { $msg .= '
<b>'; $msg .= 'Vous ne pouvez pas appliquer cette activit� a un refus
	technique.<br />'; $msg .= 'Veuillez lancer l\'activit� rt_validate
	pour valider un RT <br />'; $msg .= '
</b>
'; $document->errorStack->push(ERROR, 'Error', array(), $msg); return
false; } $appliedStatus = Rt::$_appliedStatus; $confirmedStatus =
Rt::$_confirmedStatus; $filter = ''; $filter .= ' rt_status != \'' .
$appliedStatus . '\''; $filter .= ' AND rt_status != \'' .
$confirmedStatus . '\''; $filter .= ' AND rt_access_code < 15'; $filter
.= ' AND from_version <= ' . $document; $select = array(); $rtRs =
$rt->GetRtApplyToDocument($document, false, true, $select, $filter); //
var_dump($appliedStatus, $confirmedStatus, $rtRs->RowCount());die; //
var_dump($appliedStatus, $confirmedStatus, $rtRs->RowCount());die; $li =
''; if ( $rtRs ) { while( $row = $rtRs->FetchRow() ) { $aid = null;
$rtId = $row['rt_id']; $href =
'/rbsier/document/manager/multichangestate?step=step1&checked[]=' .
$rtId . '&activityId=' . $aid; if ( $row['rt_status'] != 'rt_applied' &&
$row['rt_status'] != 'rt_confirmed' ) { $li .= '
<li>' . $row['rt_number']; $li .= ' <a href="' . $href . '"> Vous
		devriez appliquer la tache rt_validate sur ce refus et relancer </a>';
	$li .= '
</li>
'; } } if ( $li ) { $msg = $document->GetNumber() . ' :
<br />
'; $count = $rtRs->RowCount(); $msg .= "il y a $count refus applicable a
ce document:
<br />
"; $msg .= $li; $msg .= '
<ul>'; $msg .= '
</ul>
'; $msg .= 'Vous devez les valider avant des lancer ce process
<br />
'; $document->errorStack->push(ERROR, 'Error', array(), $msg); return
false; } } return $msg; } /** * * Valide les refus techniques * * @param
Document\Version $document * @return string|false Message to display */
public function rtValidate(Document\Version $document) { $msg = true;
require_once './class/Rt.php'; // Check that document is a doctype
$doctypeId = $document->GetProperty('doctype_id'); if ( $doctypeId !=
Rt::$_doctype_id ) { $msg = 'This document is not a rt';
$document->errorStack->push(ERROR, 'Error', array(), $msg); return
false; } $document->ChangeState('rt_applied');
$document->LockDocument(10); // validate return $msg; } /** * * Confirme
le rt * * @param Document\Version $document * @return string|false
Message to display */ public function confirmRt(Document\Version
$document, $instance, $successNextActivity, $faultNextActivity) { //
$container =& $document->GetContainer(); $space = & $document->space;

$rt = new Rt(); $filter = ' rt_status != \'' . Rt::$_confirmedStatus .
'\''; $filter .= ' AND rt_access_code < 15'; $select = array(); $rs =
$rt->GetRtApplyToDocument($document, false, true, $select, $filter); if
( !$rs ) return true; $i = 0; $validTag = true; $reports = array();

while( $row = $rs->FetchRow() ) { if ( $row['rt_status'] == 'rt_applied'
) { // only on applied rt $valid =
$_REQUEST['rt_validation'][$row['rt_id']]; $rtDoc = new document($space,
$row['rt_id']); if ( $valid ) { $rtDoc->ChangeState('rt_confirmed'); }
else { $reports[] = 'Le refus ' . $row['rt_number'] . ' n\'est pas
correctement appliqu� sur ce document'; $rtDoc->LockDocument(0); // Lock
document access with special code $rtDoc->ChangeState('rt_rejected');
$validTag = false; } } $i++; } if ( !$validTag ) {
$document->LockDocument(0); // Lock document access with special code
$document->ChangeState('rejete'); // Update state of the document

$instance->setNextActivity($faultNextActivity); $instance->complete();
$instance->set('check', 'reject'); } else {
$instance->setNextActivity($successNextActivity); $instance->complete();
$instance->set('check', 'approve'); } return $reports; } } /* End of
class*/
