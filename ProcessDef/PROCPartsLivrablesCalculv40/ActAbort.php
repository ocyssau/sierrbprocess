<?php
namespace ProcessDef\PROCPartsLivrablesCalculv40;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity abort
 *
 * @package PartsLivrablesCalcul40
 * @generated 2017-07-05T13:07:27+0200
 * @author
 */
class ActAbort extends AbstractActivity
{
}

