<?php
namespace ProcessDef\PROCPartsLivrablesCalculv40;

use Docflow\Service\Workflow\Callback\AbstractForm;

/**
 * A class for activity form verification_cao
 *
 * @package PartsLivrablesCalcul40
 * @generated 2017-07-05T13:07:27+0200
 * @author
 */
class FormVerificationcao extends AbstractForm
{

	const BIND_ON_VALIDATE = 0;

	const BIND_MANUAL = 1;

	const VALIDATE_ALL = 16;

	const VALUES_NORMALIZED = 17;

	const VALUES_RAW = 18;

	const VALUES_AS_ARRAY = 19;
}

