<?php

namespace ProcessDef\PROC3992693514v10;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity codrddcheck
 *
 * @package 399269351410
 * @generated 2018-01-11T15:48:30+0100
 * @author
 */
class ActCodrddcheck extends AbstractActivity
{

    /**
     * {@inheritDoc}
     * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger()
     */
    public function trigger(\Workflow\Model\Event $event)
    {
        parent::trigger($event);
    }


}

