<?php

namespace ProcessDef\PROC3992693514v10;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity end
 *
 * @package 399269351410
 * @generated 2018-01-11T15:00:23+0100
 * @author
 */
class ActEnd extends AbstractActivity
{

    /**
     * {@inheritDoc}
     * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger()
     */
    public function trigger(\Workflow\Model\Event $event)
    {
        parent::trigger($event);
    }


}

