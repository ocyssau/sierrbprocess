<?php
// %LICENCE_HEADER%
namespace ProcessDef;

use Docflow\Service\Workflow\Callback;
use Rbplm\Ged\Document;
use Rbplm\People;
use Docflow\Service\Workflow\Callback\InformationException;
use Docflow\Service\Workflow\Callback\CallbackException;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Rbs\Space\Factory as DaoFactory;

/**
 */
class ActivityHelper
{

	const ERRCODE_OLDER = 10000;

	const ERRCODE_NOFILE = 10010;

	/**
	 *
	 * @var Callback\AbstractActivity
	 */
	protected $activity;

	/**
	 *
	 * @param Callback\AbstractActivity $activity        	
	 */
	public function __construct(Callback\AbstractActivity $activity)
	{
		$this->activity = $activity;
	}

	/**
	 * 
	 * @return \ProcessDef\ActivityHelper
	 */
	public function notify($users, $subject, $htmlBody)
	{
		/* Send message to next users */
		$from = People\CurrentUser::get();
		$cc = '';
		$priority = 3;

		/**/
		$htmlBody = '<p>' . $htmlBody . '</p>';
		$htmlPart = new MimePart($htmlBody);
		$htmlPart->type = "text/html";
		$body = new MimeMessage();
		$body->addPart($htmlPart);

		/**/
		$factory = DaoFactory::get();
		
		/* @var $ranchbe \Ranchbe */
		$ranchbe = \Ranchbe::get();
		$mailService = $ranchbe->getServiceManager()->getMailService();
		$message = $mailService->messageFactory();
		$message->setFrom($from->getMail(), $from->getLogin());
		$message->setSubject($subject);
		$message->setBody($body);
		$userDao = $factory->getDao(\Rbplm\People\User::$classId);

		$toMail = null;
		$toLogin = null;
		foreach( $users as $login ) {
			$stmt = $userDao->query(array(
				'mail',
				'login'
			), 'login=:login', array(
				':login' => $login
			));
			$u = $stmt->fetch(\PDO::FETCH_ASSOC);
			if ( $u ) {
				$toMail = $u['mail'];
				$toLogin = $u['login'];
				$message->addTo($toMail, $toLogin);
			}
		}
		if ( $toMail ) {
			try{
				$mailService->send($message);
			}
			catch( \Exception $e ) {
				$ranchbe->log(sprintf('Error Code : %s: %s', $e->getCode(), $e->getMessage() ));
			}
		}

		return $this;
	}

	/**
	 * 
	 * @param Document\Version $document
	 * @return string
	 */
	public function getDetailUrl(Document\Version $document)
	{
		/* @var \Zend\Mvc\Router\Http\TreeRouteStack $router */
		$router = $this->activity->serviceManager->get('router');
		
		$url = $router->assemble([
			'spacename' => strtolower($document->spacename),
			'id' => $document->getId()
		], [
			'name' => 'ged-document-detail'
		]);

		return $url;
	}

	/**
	 * 
	 * @param Document\Version $document
	 * @return string
	 */
	public function getRunActivityUrl(Document\Version $document)
	{
		/* @var \Zend\Mvc\Router\Http\TreeRouteStack $router */
		$router = $this->activity->serviceManager->get('router');
		
		$url = $router->assemble([], [
			'name' => 'docflow'
		]);

		$queryDatas = [
			'checked[]' => $document->getId(),
			'spacename' => strtolower($document->spacename)
		];
		
		$runActivityUrl = $url . '?' . http_build_query($queryDatas);
		
		return $runActivityUrl;
	}

	/**
	 * Check mtime shift between main file and file
	 *
	 * @param Document\Version $document        	
	 * @param string $filetype
	 *        	Extension of file to test with .
	 * @param integer $maxDiff        	
	 * @return ActivityHelper
	 */
	protected function checkDate($document, $filetype, $maxDiff = 30)
	{
		/* Get the main file */
		/** @var \Rbplm\Ged\Docfile\Version $mainDocfile*/
		$mainDocfile = reset($document->getDocfiles());

		/* if none docfile, get out */
		if ( !is_object($mainDocfile) ) {
			throw new InformationException('None docfiles founded');
		}

		/** @var \Rbplm\Vault\Record $mainData */
		$mainData = $mainDocfile->getData();
		$referenceFile = $mainData->fullpath;
		$checkedFile = $mainfile->getData()->path . '/' . $mainfile->getData()->rootname . $filetype;

		if ( is_file($checkedFile) ) {
			$checkedMtime = filemtime($checkedFile);

			if ( is_file($referenceFile) ) {
				$referenceMtime = filemtime($referenceFile);
			}
			else {
				$referenceMtime = $mainData->mtime;
			}
			$timeShift = $referenceMtime - $checkedMtime;
			if ( $timeShift > $maxDiff ) {
				$msg = sprintf('The file %s has a mtime %s seconds older than the main file %s', $checkedFile, $maxDiff, $referenceFile);
				throw new CallbackException($msg, self::ERRCODE_OLDER);
			}
		}
		else {
			throw new InformationException("File $filename is not existing", self::ERRCODE_NOFILE);
		}
		return $this;
	}

	/**
	 *
	 * Verifie la presence et les dates du qcseal
	 *
	 * @param Document\Version $document        	
	 * @return string|false Message to display
	 */
	public function checkQcseal(Document\Version $document)
	{
		$msg = true;
		/* Recupere les fichiers associes au document */
		$afiles = $document->GetAssociatedFiles();
		$mainfiles = $afiles[0]['file_path'] . '/' . $afiles[0]['file_name'];

		if ( !($afiles[0]['file_extension'] == '.CATPart' || $afiles[0]['file_extension'] == '.CATProduct' || $afiles[0]['file_extension'] == '.CATDrawing') ) {
			return true;
		}

		/* Verifie la presence du qcseal */
		if ( !is_file("$mainfiles" . ".qcseal") ) {
			/*
			 * print "<b>INFOS: </b><br/>";
			 * print "<b>le fichier .qseal est abscent<b/><br/>";
			 * print "Vous devez generer le fichier .qcseal et le lier a ce document pour pouvoir executer cette tache<br/>";
			 * print '<form><input type="button" onclick="window.close()" value="Close"></form><br />';
			 */
			$msg = 'Le fichier .qseal est abscent';
			$msg .= 'Vous devez generer le fichier .qcseal et le lier a ce document pour pouvoir executer cette tache';
			$document->errorStack->push(ERROR, 'Error', array(), $msg);
			return false;
		}

		/* Verifie la concordance des dates du qcseal */
		$date_xmlf = filemtime("$mainfiles" . '.qcseal');
		$date_mainf = filemtime("$mainfiles");
		$time_shift = $date_xmlf - $date_mainf;
		if ( $time_shift < -2 ) {
			$msg = 'Le fichier qchecker nest pas a jour';
			$msg .= 'Vous devez regenerer le fichier qchecker pour pouvoir executer cette tache';
			$msg .= '<i>mtime du fichier qchecker</i>: ' . $date_xmlf . 'sec <i>mtime du fichier catia: </i>' . $date_mainf . 'sec <br />';
			$msg .= '<i>ecart de temps:</i> ' . $time_shift . 'sec <br />';
			$document->errorStack->push(ERROR, 'Error', array(), $msg);
			return false;
		}

		/* @TODO : Verifier les erreurs dans le fichiers qchecker */

		return $msg;
	}

	/**
	 *
	 * Verifie la presence et les dates du qcseal
	 *
	 * @param Document\Version $document        	
	 * @return string|false Message to display
	 */
	public function checkTubex(Document\Version $document)
	{
		$msg = true;
		/* Recupere les fichiers associes au document */
		$afiles = $document->GetAssociatedFiles();
		$mainfiles = $afiles[0]['file_path'] . '/' . $afiles[0]['file_name'];

		if ( $afiles[0]['file_extension'] == '.CATProduct' ) {
			$tubex_files[] = $afiles[0]['file_path'] . '/' . $afiles[0]['file_root_name'] . '.tubexc.xml';
			$tubex_files[] = $afiles[0]['file_path'] . '/' . $afiles[0]['file_root_name'] . '.tubex.xml';

			foreach( $tubex_files as $tubex_file ) {
				/* Verifie la presence du tubex */
				if ( is_file($tubex_file) ) {
					/* Verifie la concordance des dates */
					$date_tubex = filemtime($tubex_file);
					$date_mainf = filemtime($mainfiles);
					$time_shift = $date_tubex - $date_mainf;
					if ( $time_shift < -30 ) {
						$msg = 'Le fichier ' . $tubex_file . ' nest pas a jour';
						$msg .= 'Vous devez regenerer le fichier tubex pour pouvoir executer cette tache';
						$msg .= '<i>mtime du fichier tubex</i>: ' . $date_tubex . 'sec <i>mtime du fichier catia: </i>' . $date_mainf . 'sec <br />';
						$msg .= '<i>ecart de temps:</i> ' . $time_shift . 'sec <br />';
						$document->errorStack->push(ERROR, 'Error', array(), $msg);
						return false;
					}
				}
			}
		}

		return $msg;
	}
} /* End of class*/