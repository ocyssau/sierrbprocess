<?php
namespace ProcessDef\PROCPartsLivrablesv30;

use Docflow\Service\Workflow\Callback\AbstractActivity;
use Docflow\Service\Workflow\Callback\CallbackException;

/**
 * A class for activity Livrer
 *
 * @package PartsLivrables30
 * @generated 2017-07-03T14:47:52+0200
 * @author
 */
class ActLivrer extends AbstractActivity
{

	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger($e)
	 */
	public function trigger(\Workflow\Model\Event $event)
	{
		/** @var \Rbplm\Ged\Document\Version $document */
		$document = $event->getEmitter()->document;
		$document->lock(\Rbplm\Ged\AccessCode::INWORKFLOW);
		$document->dao->save($document);
		
		$delivreRepositPath = '/DATA6/livraison/a_livrer/ranchbe';
		
		/* Copy files to delivre reposit */
		if ( is_dir($delivreRepositPath) ) {
			/* Recupere les fichiers associes au document */
			$docfiles = $document->getDocfiles();
			/** @var \Rbplm\Ged\Docfile\Version $mainDocfile */
			$mainDocfile = reset($docfiles);
			/** @var \Rbplm\Ged\Docfile\Version $docfile */
			foreach( $docfiles as $docfile ) {
				$data = $docfile->getData();
				$data->getFsdata()->copy($delivreRepositPath);
			}
			
			/* 
			 * Corrige bug primes qui bloque les import de fichier dont le mtime du xml est dans la meme seconde
			 * que le mtime de fichier CATIA
			 */
			$mainfiles = $delivreRepositPath . '/' . $mainDocfile->getData()->getName();
			$qcsealFile = $mainfiles . '.qcseal';
			if ( is_file($qcsealFile) ) {
				$mtimeMainfile = filemtime($mainfiles);
				touch($qcsealFile, $mtimeMainfile + 2);
			}
		}
		else {
			throw new CallbackException($delivreRepositPath . ' is not existing');
		}
		
		return parent::trigger($event);
	}
}

