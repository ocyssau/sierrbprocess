<?php
namespace ProcessDef\PROCPartsLivrablesv30;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity Verifier
 *
 * @package PartsLivrables30
 * @generated 2017-07-03T14:47:52+0200
 * @author
 */
class ActVerifier extends AbstractActivity
{

	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger($e)
	 */
	public function trigger(\Workflow\Model\Event $event)
	{
		parent::trigger($event);
	}
}

