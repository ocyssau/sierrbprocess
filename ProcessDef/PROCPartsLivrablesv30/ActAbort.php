<?php
namespace ProcessDef\PROCPartsLivrablesv30;

use Docflow\Service\Workflow\Callback\AbstractActivity;
use Workflow\Model\Wf;

/**
 * A class for activity abort
 *
 * @package PartsLivrables30
 * @generated 2017-07-03T14:47:52+0200
 * @author
 */
class ActAbort extends AbstractActivity
{

	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger($e)
	 */
	public function trigger(\Workflow\Model\Event $event)
	{
		$document = $event->getEmitter()->document;
		$factory = $this->document->dao->factory;
		$procInst = $event->getEmitter()->instance;
		
		/** @var \Rbs\Wf\Instance\DocumentLinkDao $wfDocLinkDao */
		//$wfDocLinkDao = $factory->getDao(\Rbs\Wf\Instance\DocumentLink::$classId);
		//$wfDocLinkDao->deleteFromDocumentId($this->document->getId());
		
		/* close current instance */
		$procInst->setStatus(Wf\Instance::STATUS_ABORTED)->setEnded(new \DateTime());
		$factory->getDao($procInst)->save($procInst);
		
		/** @var \Rbplm\Ged\Document\Version $document */
		$document->lifeStage = 'aborted';
		$document->unlock();
		$document->dao->save($document);
		
		return parent::trigger($event);
	}
}

