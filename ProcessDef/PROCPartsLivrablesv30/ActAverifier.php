<?php
namespace ProcessDef\PROCPartsLivrablesv30;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity a_verifier
 *
 * @package PartsLivrables30
 *          @generated 2017-07-03T14:47:52+0200
 * @author
 *
 */
class ActAverifier extends AbstractActivity
{

	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger($e)
	 */
	public function trigger(\Workflow\Model\Event $event)
	{
		/** @var \Rbplm\Ged\Document\Version $document */
		$document = $event->getEmitter()->document;
		$document->lock(\Rbplm\Ged\AccessCode::INWORKFLOW);
		$document->dao->save($document);
		
		/* Recupere les fichiers associes au document */
		$docfiles = $document->getDocfiles();
		$mainfile = reset($docfiles);
		
		/* Send a message to next users */
		$documentNumber = $document->getNumber();
		$subject = $documentNumber . ' a verifier';
		$body = 'le plan ' . $documentNumber . '-' . sprintf("(indice SI%02s)", $document->version) . ' est a verifier<br>';
		// $instance->setMessageToNextUsers($subject, $body);
		
		return parent::trigger($event);
	}
}

