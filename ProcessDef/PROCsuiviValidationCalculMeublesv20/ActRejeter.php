<?php
namespace ProcessDef\PROCsuiviValidationCalculMeublesv20;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity rejeter
 *
 * @package suiviValidationCalculMeubles20
 * @generated 2017-02-22T18:03:38+0100
 * @author
 */
class ActRejeter extends AbstractActivity
{

	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger($e)
	 */
	public function trigger(\Workflow\Model\Event $event)
	{
		parent::trigger($event);
	}
}

