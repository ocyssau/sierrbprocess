<?php
namespace ProcessDef\PROC2288518315v10;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity cvecheck
 *
 * @package 228851831510
 * @generated 2018-01-09T13:54:31+0100
 * @author
 */
class ActCvecheck extends AbstractActivity
{

	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger()
	 */
	public function trigger(\Workflow\Model\Event $event)
	{
		parent::trigger($event);
	}
}

