<?php
namespace ProcessDef\PROC2288518315v10;

use Docflow\Service\Workflow\Callback\AbstractActivity;

/**
 * A class for activity documentvalidated
 *
 * @package 228851831510
 * @generated 2018-01-09T13:55:23+0100
 * @author
 */
class ActDocumentvalidated extends AbstractActivity
{

	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger()
	 */
	public function trigger(\Workflow\Model\Event $event)
	{
		/** @var \Rbplm\Ged\Document\Version $document */
		$document = $event->getEmitter()->document;
		$document->lock(\Rbplm\Ged\AccessCode::VALIDATE);
		$document->dao->save($document);
		parent::trigger($event);
	}
}

