<?php
namespace ProcessDef\PROC2140693952v10;

use Docflow\Service\Workflow\Callback\AbstractActivity;
use Docflow\Model\Event;

/**
 * A class for activity start
 *
 * @package 214069395210
 * @generated 2018-01-16T14:22:38+0100
 * @author
 */
class ActStart extends AbstractActivity
{

	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger()
	 */
	public function trigger(Event $event)
	{
		parent::trigger($event);
	}
}

