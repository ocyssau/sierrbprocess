<?php
namespace ProcessDef\PROC2140693952v10;

use Docflow\Service\Workflow\Callback\AbstractActivity;
use Docflow\Model\Event;


/**
 * A class for activity todeprecated
 *
 * @package 214069395210
 * @generated 2018-01-16T14:29:42+0100
 * @author
 */
class ActTodeprecated extends AbstractActivity
{

	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger()
	 */
	public function trigger(Event $event)
	{
		$document = $event->getEmitter()->document;
		$document->lock(\Rbplm\Ged\AccessCode::FREE);
		$document->dao->save($document);
		parent::trigger($event);
	}
}