<?php
namespace ProcessDef\PROC2140693952v10;

use Docflow\Service\Workflow\Callback\AbstractActivity;
use Docflow\Model\Event;

/**
 * A class for activity valide
 *
 * @package 214069395210
 * @generated 2018-01-16T14:15:09+0100
 * @author
 */
class ActValide extends AbstractActivity
{

	/**
	 * {@inheritDoc}
	 * @see \Docflow\Service\Workflow\Callback\AbstractActivity::trigger()
	 */
	public function trigger(Event $event)
	{
		/** @var \Rbplm\Ged\Document\Version $document */
		$document = $event->getEmitter()->document;
		//$document->lock(\Rbplm\Ged\AccessCode::FREE);
		$document->setLifeStage('valide');
		$document->dao->save($document);
		parent::trigger($event);
	}
}