<?php

namespace ProcessDef\PROC2140693952v10;

use Docflow\Service\Workflow\Callback\AbstractForm;

/**
 * A class for activity form valide
 *
 * @package 214069395210
 * @generated 2018-01-16T14:15:09+0100
 * @author
 */
class FormValide extends AbstractForm
{

    const BIND_ON_VALIDATE = 0;

    const BIND_MANUAL = 1;

    const VALIDATE_ALL = 16;

    const VALUES_NORMALIZED = 17;

    const VALUES_RAW = 18;

    const VALUES_AS_ARRAY = 19;


}

